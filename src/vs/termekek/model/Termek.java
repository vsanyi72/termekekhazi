package vs.termekek.model;

public class Termek {
	private int id;
	private String nev;
	private int ar;
	public Termek(int id, String nev, int ar) {
		super();
		this.id = id;
		this.nev = nev;
		this.ar = ar;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNev() {
		return nev;
	}
	public void setNev(String nev) {
		this.nev = nev;
	}
	public int getAr() {
		return ar;
	}
	public void setAr(int ar) {
		this.ar = ar;
	}

}

package vs.termekek.megjelenites;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Scanner;

import vs.termekek.model.Termek;
import vs.termekek.muveletek.FileMuveletek;
import vs.termekek.muveletek.TermekParser;

public class Start {
	private FileMuveletek fileMuveletek;
	private TermekParser termekParser;

	public static void main(String[] args) {

		Start start = new Start();
		start.run();

	}

	public Start() {
		this.termekParser = new TermekParser();
		this.fileMuveletek = new FileMuveletek("/work/java02/termekek.txt", this.termekParser);
	}

	private void run() {

		Scanner sc = new Scanner(System.in);
		boolean kilep = false;

		while (!kilep) {
			System.out.println("Menü");
			System.out.println("Válassz a lehetőségek közül");
			System.out.println("1, Termékek listázása");
			System.out.println("2, Új termék");
			System.out.println("3, Termék módosítása");
			System.out.println("4, Termék törlése");
			System.out.println("5, Kilépés");

			int menu = sc.nextInt();

			switch (menu) {
			case 1:
				termekLista();
				break;
			case 2:
				termekFelvetele(sc);
				break;
			case 3:
				termekModositas(sc);
				break;
			case 4:
				termekTorles(sc);
				break;
			case 5:
				kilep();
				break;
			default:
				System.out.println("Nem megfelelő menüpont");
				break;
			}

		}
		sc.close();

	}

	private void termekFelvetele(Scanner sc) {
		sc.nextLine(); // felesleges \n leolvasás
		System.out.println("Termek felvétele");
		System.out.println("id:");
		int id = sc.nextInt();
		sc.nextLine(); // ~n olvasás
		System.out.println("Terméknév:");
		String nev = sc.nextLine();
		System.out.println("Ár:");
		int ar = sc.nextInt();

		try {
			// kiolvasom a fileból az adatot
			List<Termek> termekek = this.fileMuveletek.read();

			// hozzáadom az adathoz a bekért diákot
			termekek.add(new Termek(id, nev, ar));
			// visszaírom az adatot a fileba
			this.fileMuveletek.write(termekek);
			System.out.println("A termék hozzáadva");
		} catch (NumberFormatException | ParseException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}

	private void termekModositas(Scanner sc) {
		// TODO: Megírni
		// id bekérése
		sc.nextLine(); // felesleges \n leolvasás
		System.out.println("Termék adatainak módosítása");
		System.out.println("id:");
		int id = sc.nextInt();
		// list lekérése a fileból
		List<Termek> termekek;
		try {
			termekek = this.fileMuveletek.read();
			Boolean talaltId = false;
			for (Termek termek : termekek) {
				if (termek.getId() == id) {
					System.out.println(
							"A nyilvántartott adatok : " + "\t" + termek.getNev() + "\t" + termek.getAr() +" Ft");
					// új adatok bekérése
					sc.nextLine(); // ~n olvasás
					System.out.println("Az új név:");
					String nev = sc.nextLine();
					System.out.println("Az új ár:");
					int ar = sc.nextInt();
					// bekért id-u diák kicserélése a listből
					termek.setNev(nev);
					termek.setAr(ar);
					// list filebaírása
					this.fileMuveletek.write(termekek);
					System.out.println("A " + id + " id-jű termék adatai lemódosítva");
					talaltId = true;
				}
			}
			if (talaltId == false) {
				System.out.println("Nincs ilyen ID-jű termék.");
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void termekTorles(Scanner sc) {
		// TODO: Megírni
		// id bekérése
		sc.nextLine(); // felesleges \n leolvasás
		System.out.println("Termek törlése: ");
		System.out.println("id:");
		int id = sc.nextInt();
		// list lekérése a fileból
		List<Termek> termekek;
		try {
			termekek = this.fileMuveletek.read();
			Boolean talaltId = false;
			for (Termek termek : termekek) {
				if (termek.getId() == id) {
					System.out.println(
							"A következő termék törölve a nyilvántartásból: " + "\t" 
					+ termek.getNev() + "\t" + termek.getAr()+" Ft");
					// list filebaírása
					termekek.remove(termek);
					// list filebaírás3a
					this.fileMuveletek.write(termekek);
					talaltId = true;
					break;
				}
			}
			if (talaltId == false) {
				System.out.println("Nincs ilyen ID-jű termék.");
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	private void termekLista() {
		try {
			List<Termek> termekek = this.fileMuveletek.read();
			for (Termek termek : termekek) {
				System.out.println(termek.getId() + "\t" + termek.getNev() + "\t" + termek.getAr()+" Ft");
			}
		} catch (NumberFormatException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}

	private void kilep() {
		System.exit(0);
	}
}

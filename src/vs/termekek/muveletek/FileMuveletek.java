package vs.termekek.muveletek;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import vs.termekek.model.Termek;

public class FileMuveletek implements Muveletek {

	private File file;
	private TermekParser termekParser;

	public FileMuveletek(File file, TermekParser termekParser) {
		super();
		this.file = file;
		this.termekParser = termekParser;
	}

	public FileMuveletek(String filePath, TermekParser termekParser) {
		this.file = new File(filePath);
		this.termekParser = termekParser;
	}

	@Override
	public List<Termek> read() throws NumberFormatException, IOException, ParseException {
		List<Termek> termekek = new ArrayList<>();
		String sor;
		BufferedReader br = new BufferedReader(new FileReader(this.getFile()));
		while ((sor = br.readLine()) != null) {
			termekek.add(this.getTermekParser().parseFromRow(sor.split(";")));
		}
		br.close();
		return termekek;

	}

	@Override
	public void write(List<Termek> entitys) throws FileNotFoundException {
		
		PrintWriter pw = new PrintWriter(this.getFile());
		for (Termek termek : entitys) {
			pw.write(this.getTermekParser().parseToRow(termek));
			pw.println();
		}
		pw.close();

	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public TermekParser getTermekParser() {
		return termekParser;
	}

	public void setTermekParser(TermekParser termekParser) {
		this.termekParser = termekParser;
	}

}

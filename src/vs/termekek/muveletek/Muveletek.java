package vs.termekek.muveletek;

import java.util.List;

import vs.termekek.model.Termek;

public interface Muveletek {


		List<Termek> read() throws Exception;
		void write(List<Termek> entitys) throws Exception;
		
	}

	


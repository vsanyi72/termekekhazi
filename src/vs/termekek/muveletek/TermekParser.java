package vs.termekek.muveletek;

import java.text.ParseException;

import vs.termekek.model.Termek;

public class TermekParser {

	public Termek parseFromRow(String[] splittedRow) throws NumberFormatException, ParseException {
		return new Termek(Integer.parseInt(splittedRow[0]),
					splittedRow[1],
					Integer.parseInt(splittedRow[2]));
	}

	public String parseToRow(Termek termek) {
		return new StringBuilder()
				.append(termek.getId())
				.append(";")
				.append(termek.getNev())
				.append(";")
				.append(termek.getAr())
				.toString();
	}

}
